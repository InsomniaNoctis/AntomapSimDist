﻿using MathNet.Numerics;
using MathNet.Numerics.Data.Matlab;
using MathNet.Numerics.LinearAlgebra;
using Word = Microsoft.Office.Interop.Word;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace AntomapSimilarity
{
    class FindThesaurus
    {
        List<string> domains = new List<string>();
        Matrix<double> coords = MatlabReader.Read<double>("rus4a.mat", "x");
        const double diametr = 1; //4.38 даёт плохие результаты, посмотри сам для слова "прекрасный"
        //может, стоит взять величину, исходя из средней длины вектора? А все, что больше данной величины, просто отбрасывать

        public void createDictionary() //вытаскиваем слова из "ядра" в лист
        {
            ExcelPackage ep = new ExcelPackage(new FileInfo("dic.xlsx"));
            ExcelWorksheet ws = ep.Workbook.Worksheets["dic"];
            for (int rw = 1; rw <= ws.Dimension.End.Row; rw++)
            {
                if (ws.Cells[rw, 1].Value != null)
                    domains.Add((ws.Cells[rw, 1].Value.ToString()));
            }
        }

        public Dictionary<string, double> synonymFind(string word)
        {
            Dictionary<string, double> dict = new Dictionary<string, double>();
            var wordApp = new Word.Application();
            wordApp.Visible = false;
            var infosyns = wordApp.SynonymInfo[word, Word.WdLanguageID.wdRussian];
            foreach (var iitem in infosyns.MeaningList as Array)
            {
                foreach (var item in infosyns.SynonymList[iitem] as Array)
                {
                    if (domains.Contains(item.ToString())) //есть ли слово в ядре
                    {
                        dict.Add(item.ToString(), sumSim(word, item.ToString()));
                        // var sim = sumSim(word, item.ToString());
                        //Console.WriteLine("синоним = " + item.ToString() + ", близость = " + sim.ToString() + "\n");
                    }
                    else continue;
                }

            }
            return dict;
        }
        public double sumSim(string word, string syns)
        {
            double[] sim = new double[3];
            for (int i = 0; i < 3; i++) //вычисление семантической близости по шести компонентам
            {
                sim[i] = calculateSim(coordsFind(word, i), coordsFind(syns, i));
            }
            return (sim.Sum()) / 3;
        }

        public double[] coordsFind(string word, int index)
        {
            return coords.Row((domains.IndexOf(word) + 1)).Skip(index).Take(2).ToArray();
        }

        public double calculateSim(double[] wordc, double[] syns)
        {
            return (1 - (Distance.Euclidean(syns, wordc)/diametr ));
        }

        public Dictionary<string, double> sortedListSyn(Dictionary<string, double> dict) //сортировка ассоциативного массива
        {
            return (dict.OrderByDescending(pair => pair.Value).ToDictionary(pair => pair.Key, pair => pair.Value));
        }
    }
}
