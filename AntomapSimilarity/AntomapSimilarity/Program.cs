﻿using System;
using System.Collections.Generic;

namespace AntomapSimilarity

{
    class Program
    {
        static void Main(string[] args)
        {
            FindThesaurus thes = new FindThesaurus();
            string word;
            thes.createDictionary();
            do
            {
                Console.WriteLine("Введите слово:");
                word = Console.ReadLine();
                Dictionary<string, double> res = thes.sortedListSyn(thes.synonymFind(word));
                foreach (var item in res)
                {
                    Console.WriteLine(item.Key + "=" + item.Value);
                }
            } while (word != null);
        }
    }
}
